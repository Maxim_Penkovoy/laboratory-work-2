﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParserApp
{
    public class ModifiedFieldOfSecurityThreat
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }

        public ModifiedFieldOfSecurityThreat(int id, string name, string oldValue, string newValue)
        {
            Id = id;
            Name = name;
            OldValue = oldValue;
            NewValue = newValue;
        }

        public bool CompareTo(ModifiedFieldOfSecurityThreat mf)
        {
            if (Id == mf.Id)
            {
                if (Name == mf.Name)
                {
                    if (OldValue == mf.OldValue)
                    {
                        if (NewValue == mf.NewValue)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
    }
}
