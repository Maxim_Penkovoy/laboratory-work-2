﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ParserApp
{
    /// <summary>
    /// Логика взаимодействия для NewOrDeletedThreatsTableWindow.xaml
    /// </summary>
    public partial class NewOrDeletedThreatsTableWindow : Window
    {
        private ObservableCollection<SecurityThreat> listSecurityThreats = new ObservableCollection<SecurityThreat>();
        public NewOrDeletedThreatsTableWindow(bool isNew, ObservableCollection<SecurityThreat> listThreats)
        {
            InitializeComponent();
            if (isNew)
            {
                typeOfThreatsGrid.Text = "Список добавленных угроз безопасности";
            }
            else
            {
                typeOfThreatsGrid.Text = "Список удаленных угроз безопасности";
            }
            shortInfoGrid.ItemsSource = listThreats;
            acceptButton.Click += AcceptButton_Click;
            getFullInfoButton.Click += GetFullInfoButton_Click;
        }

        private void AcceptButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void GetFullInfoButton_Click(object sender, RoutedEventArgs e)
        {
            List<SecurityThreat> fullInfo = new List<SecurityThreat>();
            foreach (var item in listSecurityThreats)
            {
                fullInfo.Add(item);
            }
            var selectedItems = shortInfoGrid.SelectedItems;
            foreach (SecurityThreat item in selectedItems)
            {
                FullInfoWindow fullInfoWindow = new FullInfoWindow(item);
                fullInfoWindow.Show();
            }
        }
    }
}
