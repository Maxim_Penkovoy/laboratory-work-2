﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Application = Microsoft.Office.Interop.Excel.Application;

namespace ParserApp
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : System.Windows.Window
    {
        private List<SecurityThreat> fullInfo = new List<SecurityThreat>();
        private List<SecurityThreat> oldFullInfo = new List<SecurityThreat>();
        private int firstIndex = 0;

        public MainWindow()
        {
            InitializeComponent();

            FirstLoadFile();

            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromHours(12);
            timer.Tick += Timer_Tick;
            timer.Start();

            numberOfRows.SelectionChanged += NumberOfRows_SelectedChanged;
            nextButton.Click += NextButton_Click;
            previousButton.Click += PreviousButton_Click;
            saveButton.Click += SaveButton_Click;
            getFullInfoButton.Click += GetFullInfoButton_Click;
            updateButton.Click += UpdateButton_Click;
        }

        private void ViewShortInfo(List<SecurityThreat> list, int numberRows, int firstIndex)
        {
            ObservableCollection<ShortInfo> shortInfoList = new ObservableCollection<ShortInfo>();
            int lastIndex = firstIndex + numberRows;
            if (lastIndex > list.Count)
            {
                lastIndex = list.Count;
            }
            for (int i = firstIndex; i < lastIndex; i++)
            {
                ShortInfo si = new ShortInfo(list[i]);
                shortInfoList.Add(si);
            }
            shortInfoGrid.ItemsSource = shortInfoList;
        }

        private void DowloadFile()
        {
            if (!Directory.Exists(@"..\..\..\User_data"))
            {
                Directory.CreateDirectory(@"..\..\..\User_data");
            }
            if (File.Exists(@"..\..\..\User_data\Info.xlsx"))
            {
                File.Delete(@"..\..\..\User_data\Info.xlsx");
            }
            WebClient wc = new WebClient();
            string url = "https://bdu.fstec.ru/files/documents/thrlist.xlsx";
            string save_path = @"..\..\..\User_data\";
            string name = "Info.xlsx";
            wc.DownloadFile(url, save_path + name);
        }       

        private void FirstLoadFile()
        {
            try
            {
                if (!File.Exists(@"..\..\..\User_data\MySaveFile.xlsx"))
                {
                    AskLoadWindow askLoadWindow = new AskLoadWindow();
                    if (askLoadWindow.ShowDialog() == true)
                    {
                        DowloadFile();
                        fullInfo = DataExcelFile.OpenExcelFile(@"..\..\..\User_data\Info.xlsx");
                    }
                }
                else
                {
                    fullInfo = DataExcelFile.OpenExcelFile(@"..\..\..\User_data\MySaveFile.xlsx");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            ViewShortInfo(fullInfo, 15, 0);
        }

        private void UpdateInfo()
        {
            UpdateWindow updateWindow;
            try
            {
                DowloadFile();
                oldFullInfo = fullInfo;
                fullInfo = DataExcelFile.OpenExcelFile(@"..\..\..\User_data\Info.xlsx");
                updateWindow = new UpdateWindow(oldFullInfo, fullInfo);
                updateWindow.Show();
            }
            catch (Exception ex)
            {
                updateWindow = new UpdateWindow(ex.Message);
                updateWindow.Show();
            }
            ViewShortInfo(fullInfo, 15, 0);
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            UpdateInfo();
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            UpdateInfo();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveWindow saveWindow = new SaveWindow();

            if (saveWindow.ShowDialog() == true)
            {  
                try
                {
                    string fullPathNewFile = DataExcelFile.MakeFullPath(saveWindow.PathFile, saveWindow.NameFile);
                    MessageBoxResult result = MessageBoxResult.Yes;
                    if (File.Exists(fullPathNewFile))
                    {
                        result = MessageBox.Show(
                            $"Файл {fullPathNewFile} уже существует.\nВы хотите заменить его?",
                            "Подтвердить сохранение",
                            MessageBoxButton.YesNo,
                            MessageBoxImage.Warning);
                    }
                    if (result == MessageBoxResult.Yes)
                    {
                        File.Delete(fullPathNewFile);
                        DataExcelFile.SaveToExcelFile(fullPathNewFile, fullInfo);
                        MessageBox.Show($"Данные сохранены в файл {fullPathNewFile}", "Сохранение", MessageBoxButton.OK, MessageBoxImage.Information);
                    }                        
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void GetFullInfoButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedItems = shortInfoGrid.SelectedItems;
            foreach (ShortInfo item in selectedItems)
            {
                SecurityThreat st = SecurityThreat.ConvertToSecurityThreat(item, fullInfo);
                FullInfoWindow fullInfoWindow = new FullInfoWindow(st);
                fullInfoWindow.Show();
            }
        }

        private void NumberOfRows_SelectedChanged(object sender, EventArgs e)
        {
            TextBlock selectedValue = (TextBlock)((ComboBoxItem)numberOfRows.SelectedItem).Content;
            ViewShortInfo(fullInfo, Convert.ToInt32(selectedValue.Text), firstIndex);
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            int numberRows = Convert.ToInt32(((TextBlock)((ComboBoxItem)numberOfRows.SelectedItem).Content).Text);
            if (firstIndex % numberRows != 0)
            {
                firstIndex = numberRows * (firstIndex / numberRows);
            }
            if ((fullInfo.Count - firstIndex) < numberRows)
            {
                numberRows = fullInfo.Count - firstIndex + 1;
            }

            if (numberRows > 0)
            {
                if (firstIndex + numberRows < fullInfo.Count)
                {
                    firstIndex += numberRows;
                    ViewShortInfo(fullInfo, numberRows, firstIndex);
                }
            }
        }

        private void PreviousButton_Click(object sender, RoutedEventArgs e)
        {
            int numberRows = Convert.ToInt32(((TextBlock)((ComboBoxItem)numberOfRows.SelectedItem).Content).Text);
            if (firstIndex % numberRows != 0)
            {
                firstIndex = numberRows * (firstIndex / numberRows);
            }
            else
            {
                firstIndex -= numberRows;
            }

            if (firstIndex >= 0)
            {
                ViewShortInfo(fullInfo, numberRows, firstIndex);
            }
            else
            {
                firstIndex = 0;
            }
        }
    }
}
