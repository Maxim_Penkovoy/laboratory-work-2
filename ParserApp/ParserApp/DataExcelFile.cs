﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;

namespace ParserApp
{
    public static class DataExcelFile
    {
        public static List<SecurityThreat> OpenExcelFile(string filePath)
        {
            List<SecurityThreat> res = new List<SecurityThreat>();
            string fullPath = System.IO.Path.GetFullPath(filePath);
            var app = new Application();
            var workBook = app.Workbooks.Open(fullPath);
            Worksheet workSheet = (Worksheet)workBook.Sheets[1];
            var lastCell = workSheet.Cells.SpecialCells(XlCellType.xlCellTypeLastCell);
            int lastColumn = lastCell.Column;
            int lastRow = lastCell.Row;

            for (int j = 3; j <= lastRow; j++)
            {
                if (workSheet.Cells[j, 1].Value == null)
                {
                    continue;
                }
                SecurityThreat st = new SecurityThreat(
                    Convert.ToInt32(workSheet.Cells[j, 1].Value.ToString()),
                    (workSheet.Cells[j, 2].Value == null) ? "" : workSheet.Cells[j, 2].Value.ToString(),
                    (workSheet.Cells[j, 3].Value == null) ? "" : workSheet.Cells[j, 3].Value.ToString(),
                    (workSheet.Cells[j, 4].Value == null) ? "" : workSheet.Cells[j, 4].Value.ToString(),
                    (workSheet.Cells[j, 5].Value == null) ? "" : workSheet.Cells[j, 5].Value.ToString(),
                    (workSheet.Cells[j, 6].Value == null) ? false : workSheet.Cells[j, 6].Value.ToString() == "1",
                    (workSheet.Cells[j, 7].Value == null) ? false : workSheet.Cells[j, 7].Value.ToString() == "1",
                    (workSheet.Cells[j, 8].Value == null) ? false : workSheet.Cells[j, 8].Value.ToString() == "1"
                    );
                res.Add(st);
            }

            workBook.Close();
            app.Quit();
            app = null;
            workBook = null;
            workSheet = null;
            GC.Collect();

            return res;
        }

        public static void SaveToExcelFile(string fullPath, List<SecurityThreat> fullInfo)
        {
            Application app = new Application();
            Workbook workBook = app.Workbooks.Add();
            Worksheet workSheet = workBook.Sheets[1];

            Range excelCellsRange1 = workSheet.Range["A1", "E1"];
            excelCellsRange1.Merge(Type.Missing);
            excelCellsRange1.Value = "Общая информация";
            Range excelCellsRange2 = workSheet.Range["F1", "H1"];
            excelCellsRange2.Merge(Type.Missing);
            excelCellsRange2.Value = "Последствия";

            Range headlineRange = workSheet.Range["A2", "H2"];
            object[,] headlineData = new object[1, 8];
            headlineData[0, 0] = "Идентификатор УБИ";
            headlineData[0, 1] = "Наименование УБИ";
            headlineData[0, 2] = "Описание";
            headlineData[0, 3] = "Источник угрозы (характеристика и потенциал нарушителя)";
            headlineData[0, 4] = "Объект воздействия";
            headlineData[0, 5] = "Нарушение конфиденциальности";
            headlineData[0, 6] = "Нарушение целостности";
            headlineData[0, 7] = "Нарушение доступности";
            headlineRange.Value = headlineData;

            var columns = 8;
            var rows = fullInfo.Count;
            Range dataRange = workSheet.Range["A3", String.Format("{0}{1}", GetExcelColumnName(columns), rows + 2)];
            object[,] data = new object[rows, columns];

            for (int rowNumber = 0; rowNumber < rows; rowNumber++)
            {
                data[rowNumber, 0] = fullInfo[rowNumber].Id;
                data[rowNumber, 1] = fullInfo[rowNumber].Name;
                data[rowNumber, 2] = fullInfo[rowNumber].Description;
                data[rowNumber, 3] = fullInfo[rowNumber].Source;
                data[rowNumber, 4] = fullInfo[rowNumber].Target;
                data[rowNumber, 5] = fullInfo[rowNumber].IsConfidentialityBreach ? "1" : "0";
                data[rowNumber, 6] = fullInfo[rowNumber].IsIntegrityBreach ? "1" : "0";
                data[rowNumber, 7] = fullInfo[rowNumber].IsAccessibilityBreach ? "1" : "0";
            }

            dataRange.Value = data;
            workBook.SaveAs(fullPath);
            workBook.Close();
            app.Quit();
            app = null;
            workBook = null;
            workSheet = null;
            GC.Collect();
        }

        public static string MakeFullPath(string pathFile, string nameFile)
        {
            if (!Directory.Exists(pathFile))
            {
                throw new DirectoryNotFoundException("Указан неверный путь к файлу!");
            }

            Regex regex = new Regex(@"[\\\/\|\:\*\?\<\>\\""]");
            MatchCollection matches = regex.Matches(nameFile);
            if (matches.Count > 0)
            {
                throw new ArgumentException("Имя файла не должно содержать следующих знаков:\n\t \\ / : * ? \" < > |");
            }

            return pathFile + nameFile + ".xlsx";
        }

        private static string GetExcelColumnName(int columnNumber)
        {
            int dividend = columnNumber;
            string columnName = null;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (int)((dividend - modulo) / 26);
            }
            return columnName;
        }

    }
}
