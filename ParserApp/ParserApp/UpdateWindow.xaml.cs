﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ParserApp
{
    /// <summary>
    /// Логика взаимодействия для UpdateWindow.xaml
    /// </summary>
    public partial class UpdateWindow : Window
    {
        private List<SecurityThreat> oldFullInfo = new List<SecurityThreat>();
        private List<SecurityThreat> newFullInfo = new List<SecurityThreat>();

        public UpdateWindow(List<SecurityThreat> oldInfo, List<SecurityThreat> newInfo)
        {
            InitializeComponent();
            oldFullInfo = oldInfo;
            newFullInfo = newInfo;
            isSuccessfully.IsChecked = true;
            ObservableCollection<ModifiedFieldOfSecurityThreat> modifiedFields = GetModifiedFields(oldFullInfo, newFullInfo);
            modifiedFieldGrid.ItemsSource = modifiedFields;
            numberOfChangedRows.Text = $"{CountNumber(modifiedFields)}";

            acceptButton.Click += AcceptButton_Click;
            addedThreatsButton.Click += AddedThreatsButton_Click;
            deletedThreatsButton.Click += DeletedThreatsButton_Click;
        }

        public UpdateWindow(string additionalInfo)
        {
            InitializeComponent();
            isError.IsChecked = true;
            numberOfChangedRows.Text = $"{0}";
            headLineModifiedFieldTextBlock.Text = "Причины ошибки";
            modifiedFieldStackPanel.Children.Remove(modifiedFieldGrid);
            modifiedFieldStackPanel.Children.Remove(modifiedFieldGrid);

            TextBlock errorTextBlock = new TextBlock();
            errorTextBlock.FontSize = 16;
            errorTextBlock.Width = 760;
            errorTextBlock.TextWrapping = TextWrapping.Wrap;
            errorTextBlock.Text = additionalInfo;
            modifiedFieldStackPanel.Children.Add(errorTextBlock);

            acceptButton.Click += AcceptButton_Click;
            addedThreatsButton.Click += AddedThreatsButton_Click;
            deletedThreatsButton.Click += DeletedThreatsButton_Click;
        }

        private int CountNumber(ObservableCollection<ModifiedFieldOfSecurityThreat> modifiedFields)
        {
            List<int> idList = new List<int>();
            foreach (var item in modifiedFields)
            {
                if (!idList.Contains(item.Id))
                {
                    idList.Add(item.Id);
                }
            }
            return idList.Count;
        }

        private ObservableCollection<ModifiedFieldOfSecurityThreat> GetModifiedFields(List<SecurityThreat> oldList, List<SecurityThreat> newList)
        {
            ObservableCollection<ModifiedFieldOfSecurityThreat> res = new ObservableCollection<ModifiedFieldOfSecurityThreat>();
            List<ModifiedFieldOfSecurityThreat> repeatEntityList = new List<ModifiedFieldOfSecurityThreat>();
            foreach (var itemOld in oldList)
            {
                foreach (var itemNew in newList)
                {
                    if (itemOld.Id == itemNew.Id)
                    {
                        if (itemOld.Name != itemNew.Name)
                        {
                            res.Add(new ModifiedFieldOfSecurityThreat(itemOld.Id, "Наименование", itemOld.Name, itemNew.Name));
                        }
                        if (itemOld.Description != itemNew.Description)
                        {
                            res.Add(new ModifiedFieldOfSecurityThreat(itemOld.Id, "Описание", itemOld.Description, itemNew.Description));
                        }
                        if (itemOld.Source != itemNew.Source)
                        {
                            res.Add(new ModifiedFieldOfSecurityThreat(itemOld.Id, "Источник", itemOld.Source, itemNew.Source));
                        }
                        if (itemOld.Target != itemNew.Target)
                        {
                            res.Add(new ModifiedFieldOfSecurityThreat(itemOld.Id, "Объект воздействия", itemOld.Target, itemNew.Target));
                        }
                        if (itemOld.IsConfidentialityBreach != itemNew.IsConfidentialityBreach)
                        {
                            res.Add(new ModifiedFieldOfSecurityThreat(
                            itemOld.Id,
                            "Нарушение конфиденциальности",
                            itemOld.IsConfidentialityBreach ? "Да" : "Нет",
                            itemNew.IsConfidentialityBreach ? "Да" : "Нет"
                            ));
                        }
                        if (itemOld.IsIntegrityBreach != itemNew.IsIntegrityBreach)
                        {
                            res.Add(new ModifiedFieldOfSecurityThreat(
                            itemOld.Id,
                            "Нарушение целостности",
                            itemOld.IsIntegrityBreach ? "Да" : "Нет",
                            itemNew.IsIntegrityBreach ? "Да" : "Нет"
                            ));
                        }
                        if (itemOld.IsAccessibilityBreach != itemNew.IsAccessibilityBreach)
                        {
                            res.Add(new ModifiedFieldOfSecurityThreat(
                            itemOld.Id,
                            "Нарушение доступности",
                            itemOld.IsAccessibilityBreach ? "Да" : "Нет",
                            itemNew.IsAccessibilityBreach ? "Да" : "Нет"
                            ));
                        }
                    }
                }
            }
            for (int i = 0; i < res.Count; i++)
            {
                for (int j = i + 1; j < res.Count; j++)
                {
                    if (res[i].CompareTo(res[j]))
                    {
                        repeatEntityList.Add(res[i]);
                    }
                }
            }
            for (int i = 0; i < repeatEntityList.Count; i++)
            {
                res.Remove(repeatEntityList[i]);
            }
            return res;
        }
        private ObservableCollection<SecurityThreat> GetNewThreats(List<SecurityThreat> oldList, List<SecurityThreat> newList)
        {
            ObservableCollection<SecurityThreat> res = new ObservableCollection<SecurityThreat>();
            foreach (var newItem in newList)
            {
                bool isNew = true;
                foreach (var oldItem in oldList)
                {
                    if (oldItem.Id == newItem.Id)
                    {
                        isNew = false;
                    }
                }
                if (isNew)
                {
                    res.Add(newItem);
                }
            }
            return res;
        }
        private ObservableCollection<SecurityThreat> GetDeletedThreats(List<SecurityThreat> oldList, List<SecurityThreat> newList)
        {
            ObservableCollection<SecurityThreat> res = new ObservableCollection<SecurityThreat>();
            foreach (var oldItem in oldList)
            {
                bool isDel = true;
                foreach (var newItem in newList)
                {
                    if (oldItem.Id == newItem.Id)
                    {
                        isDel = false;
                    }
                }
                if (isDel)
                {
                    res.Add(oldItem);
                }
            }
            return res;
        }
        private void AcceptButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void AddedThreatsButton_Click(object sender, RoutedEventArgs e)
        {
            ObservableCollection<SecurityThreat> newThreats = GetNewThreats(oldFullInfo, newFullInfo);
            NewOrDeletedThreatsTableWindow tableWindow = new NewOrDeletedThreatsTableWindow(true, newThreats);
            tableWindow.Show();
        }
        private void DeletedThreatsButton_Click(object sender, RoutedEventArgs e)
        {
            ObservableCollection<SecurityThreat> deletedThreats = GetDeletedThreats(oldFullInfo, newFullInfo);
            NewOrDeletedThreatsTableWindow tableWindow = new NewOrDeletedThreatsTableWindow(false, deletedThreats);
            tableWindow.Show();
        }
    }
}
