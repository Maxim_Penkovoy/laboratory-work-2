﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParserApp
{
    public class SecurityThreat
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Source { get; set; }
        public string Target { get; set; }
        public bool IsConfidentialityBreach { get; set; }
        public bool IsIntegrityBreach { get; set; }
        public bool IsAccessibilityBreach { get; set; }

        public SecurityThreat(int id, string name, string description, string source, string target, bool isConfidentialityBreach, bool isIntegrityBreach, bool isAccessibilityBreach)
        {
            Id = id;
            Name = name;
            Description = description;
            Source = source;
            Target = target;
            IsConfidentialityBreach = isConfidentialityBreach;
            IsIntegrityBreach = isIntegrityBreach;
            IsAccessibilityBreach = isAccessibilityBreach;
        }
        public static SecurityThreat ConvertToSecurityThreat(ShortInfo si, List<SecurityThreat> list)
        {
            SecurityThreat st = null;
            int id = Convert.ToInt32(si.Id.Substring(4));
            foreach (var item in list)
            {
                if (item.Id == id)
                {
                    st = item;
                }
            }
            return st;
        }

    }

    public struct ShortInfo
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public ShortInfo(SecurityThreat st)
        {
            Id = "УБИ." + st.Id;
            Name = st.Name;
        }
    }
}
