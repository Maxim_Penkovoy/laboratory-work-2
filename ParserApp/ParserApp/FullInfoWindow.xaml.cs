﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ParserApp
{
    /// <summary>
    /// Логика взаимодействия для FullInfo.xaml
    /// </summary>
    public partial class FullInfoWindow : Window
    {

        public FullInfoWindow(SecurityThreat sThreat)
        {
            InitializeComponent();
            acceptButton.Click += AcceptButton_Click;
            idThreat.Text = $"{sThreat.Id}";
            nameThreat.Text = sThreat.Name;
            descriptionThreat.Text = sThreat.Description;
            sourceThreat.Text = sThreat.Source;
            targetThreat.Text = sThreat.Target;

            if (sThreat.IsConfidentialityBreach)
            {
                isTrueConfidentialityBreachThreat.IsChecked = true;
            }
            else
            {
                isFalseConfidentialityBreachThreat.IsChecked = true;
            }

            if (sThreat.IsIntegrityBreach)
            {
                isTrueIntegrityBreachThreat.IsChecked = true;
            }
            else
            {
                isFalseIntegrityBreachThreat.IsChecked = true;
            }

            if (sThreat.IsAccessibilityBreach)
            {
                isTrueAccessibilityBreachThreat.IsChecked = true;
            }
            else
            {
                isFalseAccessibilityBreachThreat.IsChecked = true;
            }
        }
        private void AcceptButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
